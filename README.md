# BI Provisioning

## What is it?
Is the provisioning project for Business Intelligence projects like bi-opcions-inventory, bi-somconnexio-inventori, etc.
You have all information for deployment in the inventory projects.

## Witch programs contains?
It contains:
- PostgreSQL with Foreign Data Wrapper
- Superset - Data visualization (docker)
- Graphana - Data visualization (docker, optional) 
- Dbt - Data transform (python package)
- Crontab - Scheduling dbt processes
_- Airlfow - Python data transform (docker, optional)_
- ReportServer - Data visualization (optional)
- Node Exporter - Server monitoring (docker)
- Postgres Exporter - Postgres monitoring (docker)
- Backup with restic
- SFTP

### Superset

Superset ups five containers:
- superset_app: superset application. Exposed to internet. **It has to be Health**. If it is unhealthy the docker container has to be reviewed and restarted.
- superset_worker:  It is for executing reports. It doesn't matter if it's healthy or not.
- superset_worker_beat: It is for scheduling reports. It doesn't matter if it's healthy or not.
- superset_db: internal superset database
- superset_cache: redis with superset cache

