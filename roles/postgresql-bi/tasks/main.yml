- name: Ensure PostgreSQL users are present.
  include_role:
    name: geerlingguy.postgresql
    tasks_from: users_props
  vars:
    postgresql_users:
      - name: "{{ bi_user }}"
        role_attr_flags: SUPERUSER
      - name: "{{ bi_postgresql_reader_user }}"
        password: "{{ bi_postgresql_reader_password }}"
      - name: "{{ bi_postgresql_dbt_user }}"
        password: "{{ bi_postgresql_dbt_password }}"

- include_role:
    name: geerlingguy.postgresql
    tasks_from: users
  vars:
    postgresql_users:
      - name: "{{ monitoring_postgres_exporter_pg_user }}"
        password: "{{ monitoring_postgres_exporter_pg_password }}"
  when: monitoring_postgres_exporter_enabled is defined


- include_role:
    name: geerlingguy.postgresql
    tasks_from: databases
  vars:
    postgresql_databases:
      - name: "{{ bi_dwh_database }}"


- name: Create external schema
  postgresql_schema:
    db: "{{ bi_dwh_database }}"
    name: "{{ bi_dwh_external_schema }}"
    login_user: "{{ bi_user }}"
  become: true
  become_user: "{{ bi_user }}"

- name: Create external_meta schema
  postgresql_schema:
    db: "{{ bi_dwh_database }}"
    name: "{{ bi_dwh_external_schema }}_meta"
    login_user: "{{ bi_user }}"
  become: true
  become_user: "{{ bi_user }}"


- name: reset privs public
  postgresql_privs:
    state: absent
    database: "{{ bi_dwh_database }}"
    role: "PUBLIC"
    privs: "ALL"
    objs: public
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"


- name: reset privs dbt external
  postgresql_privs:
    state: absent
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "ALL"
    objs: "{{ bi_dwh_external_schema }}"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"
  
- name: Grant create to public schema dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "CREATE"
    objs: public
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}" 
  
- name: Grant create to dwh database dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "CREATE"
    objs: "{{ bi_dwh_database}}"
    login_user: "{{ bi_user }}"
    type: database
  become: true
  become_user: "{{ bi_user }}"


- name: Grant usage to external schema dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "USAGE"
    objs: "{{ bi_dwh_external_schema }}"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"   


- name: Grant create to external schema dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "CREATE"
    objs: "{{ bi_dwh_external_schema }}"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"   


- name: Grant usage to external_meta schema dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "USAGE"
    objs: "{{ bi_dwh_external_schema }}_meta"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"


- name: Grant create to external_meta schema dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "CREATE"
    objs: "{{ bi_dwh_external_schema }}_meta"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"


- name: Grant usage to all tables from schema public dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "SELECT"
    schema: public
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"
  
  
- name: Grant usage to all tables from schema external dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: INSERT,UPDATE,DELETE,TRUNCATE,SELECT
    schema: "{{ bi_dwh_external_schema }}"
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"

- name: Grant usage to all tables from schema external_meta dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: INSERT,UPDATE,DELETE,TRUNCATE,SELECT
    schema: "{{ bi_dwh_external_schema }}_meta"
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"


- name: Grant DEFAULT ALL to all tables from schema external_meta dbt
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: INSERT,UPDATE,DELETE,TRUNCATE,SELECT
    schema: "{{ bi_dwh_external_schema }}_meta"
    objs: TABLES
    login_user: "{{ bi_user }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"  


- name: Grant DEFAULT SELECT to all tables from schema public
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_dbt_user}}"
    privs: "SELECT"
    schema: public
    objs: TABLES
    login_user: "{{ bi_user }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"  



- name: Grant DEFAULT SELECT to all tables from schema external (dbt user)
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}"
    objs: TABLES
    login_user: "{{ bi_postgresql_dbt_user }}"
    login_host: localhost
    login_password: "{{ bi_postgresql_dbt_password }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"    

- name: Grant DEFAULT SELECT to all tables from schema external_meta (dbt user)
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}_meta"
    objs: TABLES
    login_user: "{{ bi_postgresql_dbt_user }}"
    login_host: localhost
    login_password: "{{ bi_postgresql_dbt_password }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"

- name: reset privs reader external
  postgresql_privs:
    state: absent
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "ALL"
    objs: "{{ bi_dwh_external_schema }}"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"

- name: reset privs reader external_meta
  postgresql_privs:
    state: absent
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "ALL"
    objs: "{{ bi_dwh_external_schema }}_meta"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"

- name: Grant usage to all schemas
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "USAGE"
    objs: "public,{{ bi_dwh_external_schema }},{{ bi_dwh_external_schema }}_meta"
    login_user: "{{ bi_user }}"
    type: schema
  become: true
  become_user: "{{ bi_user }}"

- name: Grant usage to all tables from schema public
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: public
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"
  
  
- name: Grant usage to all tables from schema external
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}"
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"

- name: Grant usage to all tables from schema external_meta
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}_meta"
    objs: ALL_IN_SCHEMA
    login_user: "{{ bi_user }}"
    type: table
  become: true
  become_user: "{{ bi_user }}"

- name: Grant DEFAULT SELECT to all tables from schema external
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}"
    objs: TABLES
    login_user: "{{ bi_user }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"  

- name: Grant DEFAULT SELECT to all tables from schema external_meta
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: "{{ bi_dwh_external_schema }}_meta"
    objs: TABLES
    login_user: "{{ bi_user }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"


- name: Grant DEFAULT SELECT to all tables from schema public
  postgresql_privs:
    database: "{{ bi_dwh_database }}"
    role: "{{ bi_postgresql_reader_user}}"
    privs: "SELECT"
    schema: public
    objs: TABLES
    login_user: "{{ bi_user }}"
    type: default_privs
  become: true
  become_user: "{{ bi_user }}"  





